<div class="project_list">
<?php foreach ($config as $projectName => $projectData): ?>
    <div class="project">
        <p class="name"><?php echo $projectName; ?></p>
        <?php foreach ($projectData as $k1 => $v1) : ?>
            <p class="<?php echo $k1; ?>"><?php echo $k1 ; ?></p>
            <?php if (is_array($v1)) : ?>
            <?php foreach ($v1 as $k2 => $v2) : ?>
                <span><?php echo $k2 . ': ' . $v2; ?></span><br>
            <?php endforeach; ?>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
<?php endforeach; ?>
</div>
